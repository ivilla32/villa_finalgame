﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carMove : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    Transform t;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        t = GetComponent<Transform>();
        rb.velocity = transform.forward * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
        //teleport if past certain point
        if (t.position.z > 115)
        {
            t.position=new Vector3(t.position.x,t.position.y,-100);
            //Debug.Log("car teleported");
        }
    }
}
