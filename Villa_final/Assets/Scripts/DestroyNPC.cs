﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyNPC : MonoBehaviour
{
    public GameController gc;
    private GameObject npc;
    public float timeBeforeDestroy = .5f;
    private Collider npcCollider;

    public AudioSource audioS;

    
    // Start is called before the first frame update
    void Start()
    {
        audioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("NPC"))
        {
            //Debug.Log("Destroyed NPC");
            npc = other.gameObject;

            //play sfx 
            audioS.Play();

            //get movement script attached to npc
            NPCMove npcMove = (NPCMove) npc.GetComponent(typeof(NPCMove));
            //disable that npc's movement
            npcMove.stopMoving();

            //remove tag to prevent errors in npc track
            npc.tag = "Untagged";

            //get npc collider & disable it
            npcCollider = other.gameObject.GetComponent<Collider>();
            npcCollider.enabled = false;

            //reduce npc count in gamecontroller
            gc.foundNPC();

            //wait before destroy
            StartCoroutine(DestroyWait(other.gameObject));

            //disabled once wait is enabled
            /*
            //remove npc from scene
            Destroy(other.gameObject);
            //reduce npc count in gamecontroller
            gc.foundNPC();
            */
        }
    }

    //have game wait for a few seconds before destroying npc
    IEnumerator DestroyWait(GameObject g)
    {
        yield return new WaitForSeconds(timeBeforeDestroy);
        //disable if npc collider isnt disabled
        
         //remove npc from scene
        Destroy(g);
        gc.updateNPCArray();
         
       


    }
}
