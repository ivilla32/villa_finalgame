﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timerPickup : MonoBehaviour
{
    private GameController gc;
    // Start is called before the first frame update
    void Start()
    {
        //get game controller
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("player head"))
        {
            Debug.Log("player hit timer pickup");
            gc.addToTimer();
            Destroy(this.gameObject);
        }
    }
}
