﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    private int npcCount = 6;
    public float timeLeft = 45;

    public Text timerText;
    public Text npcText;

    public Transform[] spawnPoints;

    public int addTime = 10;


    private int playerSpawn;

    private GameObject player;

    public GameObject npc;

    //end screen
    private bool gameOver;
    private bool loseWin;
    private bool restart;
    public Text endText;
    public Text endMessage;
    private string endMessageString;

    //track npc script
    public Text trackingText;
    Transform transformMin = null;
    float minDistance = Mathf.Infinity;
    int currentDistance;
    GameObject[] npcs;
    GameObject closestNPC;
    //how often the track is called
    public float rateOfTrack = 0.05f;

    PlayerMovement playerScript;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        restart = false;
        loseWin = false;
        endText.text = "";
        endMessage.text = "";
        //find player gameobject
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        
        //get array of npcs
        npcs = GameObject.FindGameObjectsWithTag("NPC");
        
        //start timer
        StartCoroutine(DecreaseTimer());
        //start tracker
        StartCoroutine(trackNPC());
        //spawn player at random spawn point
        playerSpawn = Random.Range(0, (spawnPoints.Length - 1));
        Vector3 playerSpawnPos = new Vector3(spawnPoints[playerSpawn].transform.position.x, 1f, spawnPoints[playerSpawn].transform.position.z);
        //player.transform.position = spawnPoints[playerSpawn].transform.position;

        //setting random player position
        player.transform.position = playerSpawnPos;

        //get player movement script to disable on gameover
         playerScript = (PlayerMovement)player.GetComponent(typeof(PlayerMovement));


        //spawn NPCs
        for (int i = 0; i < (spawnPoints.Length); i++)
        {
            //spawn each npc at spawnpoint other than player's spawn
            if (i != playerSpawn)
            {
                Instantiate(npc);
                //set transform to x & z axis, not y
                Vector3 npcSpawnPos = new Vector3(spawnPoints[i].transform.position.x, 1f, spawnPoints[i].transform.position.z);
                //npc.transform.position = spawnPoints[i].transform.position;
                npc.transform.position = npcSpawnPos;

            }
        }
        //find npcs
        npcs = GameObject.FindGameObjectsWithTag("NPC");

        //initialize npccount
        npcCount = npcs.Length;

        //initialize texts
        timerText.text = "Time Remaining: " + timeLeft;
        npcText.text = "Hiders Remaining: " + npcCount;
        trackingText.text = "";

    }

    // Update is called once per frame
    void Update()
    {
        //restart game
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        //all npcs found, player wins
        if (npcCount == 0)
        {
            gameOver = true;
            loseWin = true;
        }
        //time runs out, player loses
        if (timeLeft == 0)
        {
            gameOver = true;
            loseWin = false;
            //stop timer & player tracker
        }
        if (gameOver)
        {
            StopAllCoroutines();
            //if won
            if (loseWin)
            {
                endText.text = "You Win!";
                endMessageString= "You found everyone hiding.\nPress R to restart";
            }
            //if lost
            else
            {
                endText.text = "Game Over";
                endMessage.text = "You didn't find everyone.\nPress R to restart";
            }
            trackingText.text = "";
            endMessage.text = endMessageString;
            //disable player movement
            playerScript.disablePlayerMovement();

            restart = true;
        }
        
    }

    IEnumerator trackNPC()
    {
        while (npcCount > 0)
        {
            //wait before doing again while npcs are left
            //(how often tracking occurs)
            yield return new WaitForSeconds(rateOfTrack);
            //Debug.Log("tracking");
            //get array of npcs
            npcs = GameObject.FindGameObjectsWithTag("NPC");

            //get closest npc
            Vector3 currentPos = player.transform.position;
            for (int i = 0; i < npcs.Length; i++)
            {
                float distance = Vector3.Distance(npcs[i].transform.position, currentPos);
                if (distance < minDistance)
                {

                    transformMin = npcs[i].transform;
                    minDistance = distance;
                    //to track distance for text
                    closestNPC = npcs[i];
                    
                }
            }
            currentDistance= Mathf.RoundToInt(Vector3.Distance(closestNPC.transform.position, currentPos));
            //update text
            trackingText.text = "Closest Hider: " + currentDistance + "m";


        }
    }

    IEnumerator DecreaseTimer()
    {
        while (timeLeft > 0)
        {
            //only run if game isnt over
            //IF STATEMENT CRASHING UNITY ON GAME OVER
            //if (!gameOver)
            //{
                yield return new WaitForSeconds(1);
                timeLeft -= 1;
                //Debug.Log(timeLeft);
                timerText.text = "Time Remaining: " + timeLeft;
            //}
        }
    }

    //update npcCount & text when NPC is found
    public void foundNPC()
    {
        npcCount -= 1;
        //update array of npcs
        //npcs = GameObject.FindGameObjectsWithTag("NPC");
        //update tracking data
        minDistance = Mathf.Infinity;
        closestNPC = null;
        //update text
        npcText.text = "Hiders Remaining: " + npcCount;
    }

    public Transform[] passSpawns()
    {
        return spawnPoints;
    }

    //called by pickup script
    public void addToTimer()
    {
        //add time
        timeLeft += addTime;
        //update text
        timerText.text = "Time Remaining: " + timeLeft;
    }

    public void updateNPCArray()
    {
        npcs = GameObject.FindGameObjectsWithTag("NPC");
    }

    
}
