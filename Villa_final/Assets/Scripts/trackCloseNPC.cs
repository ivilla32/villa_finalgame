﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trackCloseNPC : MonoBehaviour
{
    /*
     To be attached to gamecontroller
     Finds closest NPC to the player every .25 seconds (rateOfTrack) for _ seconds (duration)
     Points arrow toward that npc every second
    */

    Transform transformMin = null;
    float minDistance = Mathf.Infinity;
    GameObject[] npcs;
    GameObject player;
    public float duration;
    //how often the track is called
    public float rateOfTrack = 0.25f;
    //kept between instances for initializing duration when function is called
    private static float keptDuration;

    GameObject closestNPC;
    
    // Start is called before the first frame update
    void Start()
    {
        //get array of npcs
        npcs = GameObject.FindGameObjectsWithTag("NPC");
        //get player game object
        player = GameObject.FindGameObjectsWithTag("Player")[0];

        keptDuration = duration;
    }

    // Update is called once per frame
    void Update()
    {
        //point arrow towards nearest NPC every second while duration > 0
        
        
    }

    IEnumerator trackNPC()
    {
        while (duration > 0)
        {
            //get closest npc
            Vector3 currentPos = player.transform.position;
            for (int i = 0; i < npcs.Length; i++)
            {
                float distance = Vector3.Distance(npcs[i].transform.position, currentPos);
                if (distance < minDistance)
                {
                    
                    transformMin = npcs[i].transform;
                    minDistance = distance;
                    //to be called in update
                    closestNPC = npcs[i];
                }
            }
           //wait before doing again while timer is active
                //(how often tracking occurs)
            yield return new WaitForSeconds(rateOfTrack);
            //decrease duration time
            duration -= rateOfTrack;
        }
    }

    public bool startTracking()
    {
        //reinitialize duration
        duration = keptDuration;
        //reinitialize npcs (if some were found between initial call
        npcs = GameObject.FindGameObjectsWithTag("NPC");

        //make arrow visible

        //call ienumerator

        //return to pickup script if duration of effect is worn off
        if (duration==0) {
            //make arrow invisible before return
            
            return true;
        }
        else
        {
            //shouldnt need to be returned, as duration will always reach 0 when
            return false;
        }
    }
}
