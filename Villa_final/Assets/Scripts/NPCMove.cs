﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMove : MonoBehaviour
{

    private GameController gc;
    //for movement points
    private Vector3[] movePoints;

    //changed from GameObject[]
    private Transform[] spawnObjArray;

    //private GameObject[] spawnObjArray;

    private int randomLocation;

    

    NavMeshAgent nav;
    // Start is called before the first frame update
    void Start()
    {
        //set up references
        nav = GetComponent<NavMeshAgent>();

        //get game controller
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        //assign spawn points from gamecontroller Vector3[]
        spawnObjArray = gc.passSpawns();
        movePoints = new Vector3[spawnObjArray.Length];


        /*
        //find each spawn gameobject
        spawnObjArray = GameObject.FindGameObjectsWithTag("spawn point");
        */

        //Debug.Log(spawnObjArray);
        //go through each one, find transform, modify y value, then add to array
        for (int i = 0; i < spawnObjArray.Length; i++)
        {
            //commented out the transforms since the type was converted from gameobject[] to transform[]
            movePoints[i] = new Vector3(spawnObjArray[i]/*.transform*/.position.x, 1f, spawnObjArray[i]/*.transform*/.position.z); //if movePoints is Transform[]
            //movePoints[i] = new Vector3(spawnObjArray[i].transform.position.x, 1f, spawnObjArray[i].transform.position.z); //if movePoints is gameobject[] 
        }
        randomLocation = Random.Range(0,movePoints.Length);



        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //start NPC movement when in range of player
    private void OnTriggerEnter(Collider other)
    {
        //make sure its player in range             //make sure nav is active (hasnt been hit by flashlight)
        if (other.gameObject.CompareTag("Player")&&nav.isActiveAndEnabled)
        {
            //Debug.Log("In range of player");
            //redetermine random location
            randomLocation = Random.Range(0, movePoints.Length);
            //start moving toward that location
            nav.SetDestination(movePoints[randomLocation]);
        }
    }

    //stop NPC movement
    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Left Player's range");
        //disable movement
        //nav.enabled = false;
    }

    public void stopMoving()
    {
        nav.enabled = false;
    }
}
